function [  ] = main( rfile, gfile, numPoints, k )
    %MAIN Summary of this function goes here
    %   Detailed explanation goes here
    [robot, obstacles, goals] = readfiles(rfile, gfile);
    dim = size(robot,1);
    [V, E] = knn(goals, dim, numPoints, k);
    disp('Step 1 of 3 complete.');
    [V, E] = getPath(obstacles, robot, goals, V, E);
    disp('Step 2 of 3 complete.');
    path = cell(size(goals,2)-1,1);
    goalsV = (size(V,1)-size(goals,2)+1):size(V,1);
    for i=2:size(goalsV,2)
        [ cost,  route] = dijkstra(E, goalsV(1,i-1), goalsV(1,i));
        if cost==Inf
            disp(strcat('The', {' '},num2str(i),'th goal configuration cannot be reached using the obtained random points. Try running the program again.'));
            return;
        end
        route = fliplr(route);
        if i~=2
            route = route(:, 2:end);
        end
        path{i-1} = route;
    end
    disp('Step 3 of 3 complete.');
    buildGUI(obstacles, robot, path, V);
end

