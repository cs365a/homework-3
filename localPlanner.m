function [ result ] = localPlanner( A, B, obstacles, robot )
    %LOCALPLANNER Summary of this function goes here
    %   Detailed explanation goes here
    result = 0;
    vector = B-A;
    vector = vector/norm(vector);
    vector = vector*(dist(B,A)/11);
    if dist(A+vector, B) > dist(A-vector,B)
        vector = -vector;
    end
    for i=1:10
        point = bsxfun(@mod, A+vector*i, 360);
        if checkCollision(point, obstacles, robot) == 1
            result = 1;
            break;
        end
    end
end
