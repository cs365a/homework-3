function [ result ] = checkCollision( point, obs, robot )
    joints = getWorkspaceJoints(robot, point);
    arms = [joints(1:end-1,:) joints(2:end,:)];
    out = lineSegmentIntersect(obs, arms);
%     hold on;
%     plot([arms(:,1) arms(:,3)]', [arms(:,2) arms(:,4)]', 'b');
%     plot([obs(:,1) obs(:,3)]', [obs(:,2) obs(:,4)]', 'r');
    if any(out.intAdjacencyMatrix(:)) == 1 
        result = 1;
    else
        result = 0;
    end
end