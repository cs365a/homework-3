function D2 = dist(Z1, Z2)
    diff = bsxfun(@minus, Z2, Z1);
    diff = bsxfun(@mod, diff, 360);
    temp = find(diff>180);
    for i=temp
        diff(i) = 360 - diff(i);
    end
    D2 = sqrt(sum(diff.^2,2));
end