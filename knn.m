function [ V, E ] = knn(goals, numJoints, numPoints, k )
    %KNN Summary of this function goes here
    %   Detailed explanation goes here
    V = 360*rand(numPoints, numJoints);
    V = [V;goals'];
    [IDX, D] = knnsearch(V, V, 'k', k+1, 'distance', @dist);
    IDX = IDX(:, 2:end);
    D = D(:, 2:end);
    E = zeros(numPoints);
    for i=1:size(IDX,1)
        for j=1:size(IDX,2)
            E(i,IDX(i,j)) = D(i, j);
        end
    end
    %csvwrite(strcat('graph', num2str(numJoints), '_', num2str(numPoints), '_', num2str(k), '.csv'), [V E]);
end
