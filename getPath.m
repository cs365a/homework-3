function [ V, E ] = getPath( obstacles, robot, goals, V, E )
    %GETPATH Summary of this function goes here
    %   Detailed explanation goes here
    sz = size(V,1);
    i=1;
    if size(robot,1) == 2
        f = figure(1);
        hold on;
    end
    while i <= sz
        if checkCollision(V(i,:), obstacles, robot) == 1
          if size(robot,1)==2
            plot(V(i,1), V(i,2), '.k');
          end
          V(i,:) = [];
          E(i,:) = [];
          E(:,i) = [];
          sz = sz-1;
        else
            i=i+1;
        end
    end
    disp('Inititating Local Planner...');
    curr = 0;
    for i=1:size(E,1)
        for j=1:(i-1)
            if E(i,j) ~= 0
                if localPlanner(V(i,:), V(j,:), obstacles, robot) == 1
                    E(i,j) = 0;
                    E(j,i) = 0;
                end
            end
        end
        if curr <= i*100/size(E,1)
            curr = curr + 5;
            disp(strcat('Step 2 of 3:', num2str(i*100/size(E,1)), '% complete.'));
        end
    end
    if size(robot,1) == 2
        gplot(E, V, 'b');
        plot(goals(1, 2:end), goals(2, 2:end), '.m', 'Markersize', 20);
        plot(goals(1,1), goals(1,2), '.r', 'Markersize', 20);
        saveas(f, strcat('getPath', num2str(size(robot,1)),'_',num2str(size(V,1)),'.png'));
        hold off;
    end
    %csvwrite(strcat('getPath_', num2str(size(robot,1)),'_',num2str(size(V,1)),'.csv'), [V E]);
end
