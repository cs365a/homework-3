function [ output ] = getWorkspaceJoints( robot, config )
    dim = size(config, 2);
    output = zeros(dim+1, 2);
    currTheta = 0;
    for i=1:dim
        currTheta = currTheta + config(i);
        output(i+1, :) =  [robot(i)*cosd(currTheta), robot(i)*sind(currTheta)] + output(i, :);
    end
end

