function [  ] = buildGUI( obstacles, robot, path, V )
    %Builds a Graphical Interface for robot arm visualization
    figure(2);
    hold on;
    ylim([-500 499]);
    xlim([-500 499]);
    plot([obstacles(:,1) obstacles(:,3)]', [obstacles(:,2) obstacles(:,4)]', 'r');
    counter = 1;
    color = hsv(size(path,1)*10);
    color = color(randperm(size(color,1), size(path,1)),:);
    filename = strcat('animation_', num2str(size(robot,1)),'_', num2str(size(V,1)),'.gif');
    for j=1:size(path,1);
        route = path{j,1};
        for i=route
            arms = getWorkspaceJoints(robot, V(i,:));
            h1 = plot(arms(:,1)', arms(:,2)', 'color', color(j,:));
            pause(0.1);
            frame = getframe(2);
            im = frame2im(frame);
            [imind,cm] = rgb2ind(im,256);
            if j==1 && i == route(1)
                imwrite(imind,cm,filename,'gif', 'Loopcount',inf);
            else
                imwrite(imind,cm,filename,'gif','Delay', 0, 'WriteMode','append');
            end
            counter = counter + 1;
            if(i~=route(end))
                delete(h1);
            end
        end
    end
    hold off;
end