function [ robot, obstacles, goals ] = readfiles( robotFile, goalFile )
    %read robot.dat and return robot specifications and obstacles in
    %multidimensional array
    fid = fopen(robotFile);
    fgetl(fid);
    tline = fgetl(fid);
    data = textscan(tline, '%f');
    robot = data{1};
    obsNum = str2double(fgetl(fid));
    obstacles = zeros(obsNum, 4);
    for i=1:obsNum
        data1 = textscan(fgetl(fid), '%f');
        data2 = textscan(fgetl(fid), '%f');
        obstacles(i,:) = reshape(cell2mat([data1 data2]), 1, 4);
    end
    fclose(fid);
    fid = fopen(goalFile);
    tline = fgetl(fid);
    goals = [];
    while(ischar(tline))
        goals = [goals, cell2mat(textscan(tline, '%f'))];
        tline = fgetl(fid);
    end
    goals = bsxfun(@mod, goals, 360);
end

